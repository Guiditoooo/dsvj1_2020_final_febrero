#include "game.h"

namespace game {

	void play() {

		init();

		do
		{

			update();
			draw();

		} while (!WindowShouldClose());

		deinit();

	}
	
	void init() {

		InitWindow(config::screen_width, config::screen_height, &config::screen_title[0]); 
		SetTargetFPS(config::fps_rate);
		grid::init();
		grid::start();
		player::init();

	}

	void update() {

		player::update();
		grid::update();

	}

	void draw() {

		BeginDrawing();
		ClearBackground(RAYWHITE);
		
		grid::draw();
		player::draw();

		EndDrawing();
	}

	void deinit() {

		player::deinit();
		CloseWindow();

	}

}