#include "map.h"
#include "game/config/config.h"

namespace game {
	namespace grid {

		mapTile map[width][height];

		void start() {
			for (int x = 0; x < grid::width; x++)
			{
				for (int y = 0; y < grid::height; y++)
				{

					setTile(x, y);

				}
			}
			map[5][3].type = TYPE::BODY;
			setFruit();
		}

		void init() {

			for (int x = 0; x < grid::width; x++)
			{
				for (int y = 0; y < grid::height; y++)
				{
					map[x][y].rec.width = config::screen_width / grid::width;
					map[x][y].rec.height = config::screen_height/ grid::height;
					map[x][y].rec.x = map[x][y].rec.width * x;
					map[x][y].rec.y = map[x][y].rec.height * y;

				}
			}
		}
		void update() {
			
		}
		void draw() {
			for (int x = 0; x < grid::width; x++)
			{
				for (int y = 0; y < grid::height; y++)
				{

					DrawRectangleRec(map[x][y].rec,map[x][y].color);

				}
			}
			
		}

		void drawPlayer(int x, int y) {

		}

		void deinit() {

		}

		void setTile(int x, int y) {
			if (x == 0 || x == grid::width - 1 || y == 0 || y == grid::height - 1  ) {
				map[x][y].type = TYPE::WALL;
				map[x][y].color = DARKGRAY;
			}
			else
			{
				map[x][y].type = TYPE::FLOOR;
				map[x][y].color = WHITE;
			}
			
		}
		void setFruit() {

			int x = 0;
			int y = 0;

			do {
				x = GetRandomValue(1, grid::width - 1);
				y = GetRandomValue(1, grid::height - 1);
			} while (map[x][y].type != TYPE::FLOOR);

			map[x][y].type = TYPE::FRUIT;
			map[x][y].color = RED;

		}

	}
}