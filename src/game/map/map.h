#ifndef MAP_H
#define MAP_H

#include "raylib.h"

namespace game {
	namespace grid {
		const int width = 16;
		const int height = 16;

		enum class TYPE { FLOOR, HEAD, BODY, WALL, FRUIT };
		enum class DIRECTION { NONE, UP, LEFT, DOWN, RIGHT };

		struct mapTile {
			TYPE type = TYPE::FLOOR;
			Rectangle rec = {0};
			Color color = WHITE;
			DIRECTION direction = DIRECTION::UP;
			DIRECTION nextDirection = DIRECTION::UP;
			//config::Pos2 center = { 0,0 };
		};

		

		extern mapTile map[width][height];
		
		void start();
		void init();
		void update();
		void draw();
		void drawPlayer(int x, int y);
		void deinit();
		void setTile(int x, int y);
		void setFruit();

	}
}

#endif // !MAP_H
