#ifndef CONFIG_H
#define CONFIG_H

#include <iostream>

namespace config {

	const int screen_width = 800;
	const int screen_height = 800;
	const std::string screen_title = "DSVJ1_FINAL_TELLO_GUIDO";

	const int fps_rate = 60;

	const int KEY_w = KEY_W + 32;
	const int KEY_a = KEY_A + 32;
	const int KEY_s = KEY_S + 32;
	const int KEY_d = KEY_D + 32;

	struct Pos2 {
		int x;
		int y;
	};

}

#endif // !CONFIG_H