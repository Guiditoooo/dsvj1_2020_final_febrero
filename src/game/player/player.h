#ifndef PLAYER_H
#define PLAYER_H

#include "raylib.h"
#include "game/config/config.h"
#include "game/map/map.h"

namespace game {

	namespace player {

		enum class DIRECTION { NONE, UP, LEFT, DOWN, RIGHT };
			
		void init();
		void update();
		void input();
		void draw();
		void deinit();

		/*struct Player {
			config::Pos2 pos[(grid::width - 2) * (grid::height - 2)];
			int length = 3;
			DIRECTION direction = DIRECTION::UP;
			DIRECTION nextDirection = direction;
		};*/
		
		//extern Player pj;

		struct Player {
			config::Pos2 pos;
			bool move = false;
			DIRECTION direction = DIRECTION::UP;
		};

		extern Player pj[];
		extern int length;

	}

}

#endif // !PLAYER_H

