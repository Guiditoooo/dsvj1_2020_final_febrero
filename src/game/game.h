#ifndef GAME_H
#define GAME_H

#include "raylib.h"
#include "config/config.h"
#include "player/player.h"
#include "game/map/map.h"

namespace game {

	void play();
	void init();
	void update();
	void draw();
	void deinit();

}

#endif

